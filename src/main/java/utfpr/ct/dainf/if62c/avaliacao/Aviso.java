package utfpr.ct.dainf.if62c.avaliacao;

import java.util.TimerTask;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Aviso extends TimerTask {
    
    protected final Compromisso compromisso;

    public Compromisso getCompromisso() {
        return compromisso;
    }

    public Aviso(Compromisso compromisso) {
       this.compromisso = compromisso;
    }

    @Override
    public void run() {
        long tempo = (this.compromisso.getData().getTime() - System.currentTimeMillis())/1000;
        System.out.println(this.compromisso.getDescricao() + " começa em " + tempo + "s.");
    }
        
}
