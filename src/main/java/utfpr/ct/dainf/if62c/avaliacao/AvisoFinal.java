package utfpr.ct.dainf.if62c.avaliacao;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class AvisoFinal extends Aviso {

    @Override
    public void run() {
        System.out.println(this.compromisso.getDescricao() + " começa agora."); //To change body of generated methods, choose Tools | Templates.
        compromisso.getAvisos().stream().forEach((aviso) -> aviso.cancel());
    }

    public AvisoFinal(Compromisso compromisso) {
        super(compromisso);
    }
    
}
